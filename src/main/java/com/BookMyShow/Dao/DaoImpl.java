package com.BookMyShow.Dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.BookMyShow.Model.Categories;

@Repository
@Transactional(readOnly=true)
public class DaoImpl implements Dao{
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<Categories> getAll() {
			
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Categories.class);
		return criteria.list();
	}

}
