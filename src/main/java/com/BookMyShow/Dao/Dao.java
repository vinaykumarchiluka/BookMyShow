package com.BookMyShow.Dao;

import java.util.List;

import com.BookMyShow.Model.Categories;

public interface Dao {

	 abstract List<Categories> getAll();
}
