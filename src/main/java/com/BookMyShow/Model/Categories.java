package com.BookMyShow.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Categories {

	@Id
	private int id;
	private String categoryName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
}
