package com.BookMyShow.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.BookMyShow.Dao.Dao;
import com.BookMyShow.Model.Categories;

@Controller
public class MainController {

	@Autowired
	Dao dao;
 
	@RequestMapping("home")
	public ModelAndView homePage() {
		List<Categories> categoryList=dao.getAll();
		for(Categories cg: categoryList) {
			System.out.println(cg.getCategoryName());
		}
		System.out.println("helo");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("HomePage");
	    mv.addObject("categoryList", categoryList);
		return mv;
	}
	
	
}
